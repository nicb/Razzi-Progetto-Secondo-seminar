# Fausto Razzi, *Progetto Secondo* (seminar)

This is a set of slides that describes the work by Fausto Razzi *Progetto
Secondo* for synthetic sounds (1980 - reworked in 2007).

# DELIVERED

1. Fondazione Giorgio Cini - *Computer-Assisted Composition*, Venezia, December 09-10 2022

# LICENSE

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img
alt="Creative Commons License" style="border-width:0"
src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This
work is licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons
Attribution-ShareAlike 4.0 International License</a>.

Please read the [License](./LICENSE.md) for more information.
