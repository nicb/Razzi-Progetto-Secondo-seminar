BEGIN {
        FS = "|"
        OFS = "\t"
        log2 = 1/log(2)
        last = 0
} 
/^[0-9]/ {
        linear_extremes = $3/$1
        geometric_extremes = 12*(log2*log($3/$1))

        print("extremes", linear_extremes, geometric_extremes)

        if (last != 0)
        {
                central = $2/last
                geometric = 12*(log2*log($2/last))
                print("centrals", linear_extremes, geometric_extremes)
        }

        last = $2
}
